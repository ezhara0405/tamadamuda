class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def interview_provider(provider, client)
    @provider, @client = provider, client
    mail(to: provider.email, subject: 'interview')
  end

  def interview_client(client, provider)
    @provider, @client = provider, client
    mail(to: client.email, subject: 'interview')
  end
end
