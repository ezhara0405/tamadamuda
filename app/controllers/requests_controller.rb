class RequestsController < ApplicationController
  def new
    @request = current_client.requests.new
  end

  def create
    if client_signed_in?
      current_client.requests.create!(requests_params)
      redirect_to dashboard_client_path
    else
      session[:new_request] = requests_params
      redirect_to new_user_registration_path
    end
  end

  def talk
    @request = Request.find(params[:id])
  end

  def send_message
    Danthes.publish_to "/messages/new/#{params[:request_id]}", :chat_message => params[:message], from: "[#{Time.current}] #{params[:from]}"
  end

  protected

  def requests_params
    params.require(:request).permit(:service_id, :max_price, :date)
  end
end
