class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  layout 'application_inner'

  protected

  helper_method :current_provider
  def current_provider
    return current_user if current_user && current_user.provider?
  end

  helper_method :provider_signed_in?
  def provider_signed_in?
    current_provider.present?
  end

  helper_method :current_client
  def current_client
    return current_user if current_user && !current_user.provider?
  end

  helper_method :client_signed_in?
  def client_signed_in?
    current_client.present?
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email, :password, :password_confirmation, :provider) }
  end

end
