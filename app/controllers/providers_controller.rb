class ProvidersController < ApplicationController
  def dashboard
    @profile = current_provider.provider_profile || current_provider.build_provider_profile

    @requests = current_provider.possible_requests
  end

  def update_profile
    @profile = current_provider.provider_profile || current_provider.build_provider_profile

    @profile.update(profile_params)

    redirect_to dashboard_provider_path
  end

  def reject_request
    @request = Request.find(params[:id])
    ProviderFeedback.create(provider_id: current_provider.id, request: @request, state: 'rejected')
    redirect_to dashboard_provider_path
  end

  def accept_request
    @request = Request.find(params[:id])
    ProviderFeedback.create(provider_id: current_provider.id, request: @request, state: 'accepted')
    redirect_to dashboard_provider_path
  end

  def show_profile
    @profile = current_provider.provider_profile || current_provider.build_provider_profile

  end

  protected

  def profile_params
    params.require(:provider_profile).permit(:name, :phone, :description, :service_id, :urls)
  end
end
