class HomeController < ApplicationController
  layout 'application_simple'

  def landing
    if client_signed_in?
      redirect_to dashboard_client_path
    elsif provider_signed_in?
      redirect_to dashboard_provider_path
    end

    @services = Service.all.map{|s| [s.name, s.id]}
    @request = client_signed_in? ? current_client.requests.new : Request.new
  end
end
