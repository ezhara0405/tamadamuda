module Users
  class RegistrationsController < Devise::RegistrationsController
    layout 'application_simple'

    def create
      super
      if signed_in?(:user) && client_signed_in? && session[:new_request].present?
        current_client.requests.create(session[:new_request])
        session.delete(:new_request)
      end
    end

    protected

    def after_sign_up_path_for(user)
      if user.provider?
        dashboard_provider_path
      else
        dashboard_client_path
      end
    end
  end
end
