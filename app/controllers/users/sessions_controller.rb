module Users
  class SessionsController < Devise::SessionsController
    layout 'application_simple'

    protected

    def after_sign_in_path_for(user)
      if user.provider?
        dashboard_provider_path
      else
        dashboard_client_path
      end
    end
  end
end
