//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require danthes
//= require bootstrap-datepicker
//= require_tree .


$(function() {
  $('.datepicker').datepicker({
      language: 'ru',
      weekStart: 1
  });
});