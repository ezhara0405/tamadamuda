class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :requests
  has_many :provider_feedbacks
  has_one :provider_profile

  def possible_requests
    if provider? && provider_profile.present?
      rejected_requests = ProviderFeedback.where(provider_id: self.id, state: 'rejected').pluck(:request_id) || []

      res = Request.where(service_id: provider_profile.service_id).where(state: :pending)
      if rejected_requests.any?
       res = res.where('id not in (?)', rejected_requests)
      end

      res
    else
      []
    end
  end
end
