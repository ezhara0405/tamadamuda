class Request < ActiveRecord::Base
  belongs_to :user
  belongs_to :service

  state_machine :state, initial: :pending do
    event :accept do
      transition :pending => :accepted
    end

    event :cancel do
      transition :pending => :cancelled
    end
  end
end
