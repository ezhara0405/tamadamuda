class ProviderFeedback < ActiveRecord::Base
  belongs_to :provider, class_name: 'User'
  belongs_to :request
end
