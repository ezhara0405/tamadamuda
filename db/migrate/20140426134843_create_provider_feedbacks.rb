class CreateProviderFeedbacks < ActiveRecord::Migration
  def change
    create_table :provider_feedbacks do |t|
      t.integer :provider_id
      t.integer :request_id
      t.string :state

      t.timestamps
    end
  end
end
