class AddColumnUrlsToProviderProfile < ActiveRecord::Migration
  def change
    add_column :provider_profiles, :urls, :text
  end
end
