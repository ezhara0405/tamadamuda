class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :client_id
      t.integer :provider_id
      t.integer :service_id
      t.text :content

      t.timestamps
    end
  end
end
