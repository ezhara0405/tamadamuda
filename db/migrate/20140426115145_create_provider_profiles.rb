class CreateProviderProfiles < ActiveRecord::Migration
  def change
    create_table :provider_profiles do |t|
      t.string :name, null: false
      t.string :phone, null: false
      t.string :description, null: false
      t.integer :service_id, null: false
      t.integer :user_id, null: false

      t.timestamps
    end
  end
end
