class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :user_id
      t.integer :service_id
      t.string :state

      t.timestamps
    end
  end
end
