class AddColumnsToRequest < ActiveRecord::Migration
  def change
    add_column :requests, :max_price, :integer
    add_column :requests, :date, :date
  end
end
