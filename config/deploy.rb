set :application, 'evenduck.ru'

set :rvm_ruby_string, "2.0.0"

set :user, 'rails'
set :use_sudo, false
set :shared_children, shared_children + %w[tmp/sockets]
set :shared_configs, %w[database.yml]

set :scm, :git
set :repository, 'git@bitbucket.org:azhavoronkov/tamadamuda.git'
set :deploy_via, :copy
set :ssh_options, forward_agent: true
set :normalize_asset_timestamps, true

# Releases Config
set :keep_releases, 5

# Deploy configuration
set :domain_name, '188.138.108.75'

# Rails config
set :action_mailer_delivery_method, :sendmail
set :action_mailer_default_url_host, domain_name

server domain_name, :web, :app, :db, primary: true

set :assets_role, :app

set :deploy_to, "/home/rails/#{application}"
set :branch, 'master'
set :rails_env, 'production'

# Deploy tasks
namespace :deploy do

  desc "Zero-downtime restart of Unicorn"
  task :restart, roles: :app, except: { no_release: true } do
    run "kill -s USR2 `cat #{shared_path}/pids/unicorn.pid`"
  end

  desc "Start unicorn"
  task :start, roles: :app, except: { no_release: true } do
    run "cd #{current_path}; bundle exec unicorn -c config/unicorn.rb -E #{rails_env} -D"
  end

  desc "Stop unicorn"
  task :stop, roles: :app, except: { no_release: true } do
    run "kill -s QUIT `cat #{shared_path}/pids/unicorn.pid`"
  end

end

# Config management
namespace :config do

  desc "Create symlinks to shared configs"
  task :symlink, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/unicorn.rb #{release_path}/config/unicorn.rb"
  end

end

namespace :db do

  desc "Seeds database"
  task :seed, roles: :app do
    run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end

end

after 'deploy:finalize_update', 'config:symlink'

after 'deploy', 'deploy:cleanup'
after 'deploy:cold', 'deploy:cleanup'
#after 'deploy:update_code', 'deploy:assets:precompile'

require './config/boot'
