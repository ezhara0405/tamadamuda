Tamadamuda::Application.routes.draw do
  devise_for :users, module: :users
  root 'home#landing'
  resources :requests do
    member do
      get :talk
      post :send_message
    end
  end
  resource :client, only: :none do
    get :dashboard
  end
  resource :provider, only: :none do
    get :dashboard
    get :show_profile
    post :update_profile
    post :accept_request
    post :reject_request
  end
end
